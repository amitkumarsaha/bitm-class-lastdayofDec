<?php

class Cricket
{
    public function __construct()
    {
        echo "cricket class";
        echo "<br/>";
    }

}

class Movie extends Cricket
{
    public function __construct()
    {
        echo "movie class";

        parent::__construct();
    }
}

$obj = new Movie();

?>