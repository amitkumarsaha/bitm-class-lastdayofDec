<?php

interface SteveJobs
{
    public function getDevice();

}

interface BillGates{
    public function getInfo();
}

interface DonaldTrump{
    public function Personality();
}

class Person implements SteveJobs{
    public function getDevice()
    {
       echo  " Steve Jobs : chief executive officer (CEO) of Apple Inc . ";
        echo "<br/>";
    }

    public function getInfo(){
        echo "Bill Gates : co-founded Microsoft . ";
        echo "<br/>";
    }
    public function Personality(){
        echo "Donald Trump : President-elect of the United States . ";
        echo "<br/>";
    }

}

$per=new Person();
$per->getDevice();
$per->getInfo();
$per->Personality();